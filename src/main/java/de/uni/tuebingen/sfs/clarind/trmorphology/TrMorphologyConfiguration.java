package de.uni.tuebingen.sfs.clarind.trmorphology;

import io.dropwizard.Configuration;


/**
 * Turkish morphology web service configuration.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */
public class TrMorphologyConfiguration extends Configuration {
}
