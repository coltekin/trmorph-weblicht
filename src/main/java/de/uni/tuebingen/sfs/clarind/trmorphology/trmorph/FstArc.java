package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;

/**
 * A simple class representing an arc.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */
public class FstArc
{
    String inSym = null;
    String outSym = null;
    int nextNode = 0;

    public FstArc(String in, String out, int node) 
    {
        inSym = in;
        outSym = out;
        nextNode = node;
    }
}
