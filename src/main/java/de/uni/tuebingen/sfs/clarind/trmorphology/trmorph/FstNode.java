package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;

/**
 * Class representing a node in an FST.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */

public class FstNode
{
    HashMap<String,HashMap<String,HashSet<Integer>>> arcs = null;
    boolean is_final = false;

    public FstNode()
    {
        arcs = new HashMap<String,HashMap<String,HashSet<Integer>>>();
    }

    public void addArc(String inSym, String outSym, int endNode)
    {
        HashMap<String,HashSet<Integer>> out = arcs.get(inSym);
        HashSet<Integer> nodeSet = null;

        if (out == null) {
            out = new HashMap<String,HashSet<Integer>>();
            nodeSet = new HashSet<Integer>();
            nodeSet.add(endNode);
            out.put(outSym, nodeSet);
            arcs.put(inSym, out);
        } else {
            nodeSet = out.get(outSym);
            if (nodeSet == null) {
                nodeSet = new HashSet<Integer>();
                nodeSet.add(endNode);
                out.put(outSym, nodeSet);
            } else {
                nodeSet.add(endNode);
                out.put(outSym, nodeSet);
            }
        }
    }

    public FstArc [] findArcs(String inSym, boolean known)
    {
        ArrayList<FstArc> result = new ArrayList<FstArc>();
        boolean found = false;
        HashMap<String,HashSet<Integer>> outMap = null;

        if (inSym != null) {
            outMap = arcs.get(inSym);
            if (known && outMap != null) {
                for (String outSym: outMap.keySet()) {
                    for (int node: outMap.get(outSym)) {
                        result.add(new FstArc(inSym, outSym, node));
                        found = true;
                    }
                }
            } else { 
                outMap = arcs.get("@_UNKNOWN_SYMBOL_@");
                if (outMap != null) {
                    for (String outSym: outMap.keySet()) {
                        for (int node: outMap.get(outSym)) {
                            result.add(new FstArc(inSym, outSym, node));
                            found = true;
                        }
                    }
                }
                outMap = arcs.get("@_IDENTITY_SYMBOL_@");
                if (outMap != null) {
                    for (String outSym: outMap.keySet()) {
                        for (int node: outMap.get(outSym)) {
                            result.add(new FstArc(inSym, outSym, node));
                            found = true;
                        }
                    }
                }
            }
        }

        outMap = arcs.get("");
        if (outMap != null) {
            for (String outSym: outMap.keySet()) {
                for (int node: outMap.get(outSym)) {
                    result.add(new FstArc("", outSym, node));
                    found = true;
                }
            }
        }

        if (found) {
            return (result.toArray(new FstArc[result.size()]));
        } else {
            return null;
        }
    }

    public FstArc [] findArcs(String inSym)
    {
        return findArcs(inSym, true);
    }


    public boolean isFinal()
    {
        return is_final;
    }

    public void setFinal()
    {
        is_final = true;
    }

    public void writeATT(int nodenum)
    {
        for (String inSym: arcs.keySet()) {
            HashMap<String,HashSet<Integer>> out = arcs.get(inSym);
            if (out != null) {
                for (String outSym: out.keySet()) {
                    for (int next_node: out.get(outSym)){
                        if ("".equals(inSym)) inSym = "@0@";
                        if ("".equals(outSym)) outSym = "@0@";
                        System.out.format("%d\t%d\t%s\t%s\n",
                                nodenum, next_node, outSym, inSym);
                    }
                }
            }
        }
    }
}
