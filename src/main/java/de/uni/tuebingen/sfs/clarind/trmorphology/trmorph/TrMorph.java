package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;

import java.io.InputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class parses the TRmorph analysis symbols, and also converts
 * it into various tagsets.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */

public class TrMorph
{
    private final Fst fst;
    private static final Pattern re;
    private final static HashMap<String,String> posMapUD;
    private final static HashMap<String,String> posMapMS;
    static {
        posMapUD = new HashMap<String,String>();
        posMapUD.put("Alpha",    "X");
        posMapUD.put("Adj",      "ADJ");
        posMapUD.put("Adv",      "ADV");
        posMapUD.put("Cnj",      "CONJ");
        posMapUD.put("Det",      "DET");
        posMapUD.put("Exist",    "ADJ");
        posMapUD.put("Ij",       "INTJ");
        posMapUD.put("N",        "NOUN");
        posMapUD.put("Not",      "PART");
        posMapUD.put("Num",      "NUM");
        posMapUD.put("Onom",     "X");
        posMapUD.put("Postp",    "ADP");
        posMapUD.put("Prn",      "PRON");
        posMapUD.put("Punc",     "PUNCT");
        posMapUD.put("Sym",      "SYM");
        posMapUD.put("Q",        "AUX");
        posMapUD.put("V",        "VERB");

        posMapMS = new HashMap<String,String>();
        posMapMS.put("Adj",      "Adj");
        posMapMS.put("Adv",      "Adv");
        posMapMS.put("Cnj",      "Conj");
        posMapMS.put("Det",      "Det");
        posMapMS.put("Exist",    "Adj");
        posMapMS.put("Ij",       "Interj");
        posMapMS.put("N",        "Noun");
        posMapMS.put("Not",      "Verb");
        posMapMS.put("Num",      "Num");
        posMapMS.put("Onom",     "Interj");
        posMapMS.put("Postp",    "ADP");
        posMapMS.put("Prn",      "Pron");
        posMapMS.put("Punc",     "Punc");
        posMapMS.put("Sym",      "Punc");
        posMapMS.put("Q",        "Ques");
        posMapMS.put("V",        "Verb");

        re = Pattern.compile("(.+?)(<[A-Z][A-Za-z0-9:]*>)(.*?)"
                + "((<[^A-Z][A-Za-z0-9:]*><[A-Z][A-Za-z0-9:]*>.*)$|$)");
    };

    public TrMorph(InputStream in) throws IOException {
//        fst = new Fst(TrMorph.class.getResourceAsStream(in));
        fst = new Fst(in);
    }
//        fst = new Fst(TrMorph.class.getResourceAsStream(in));
    public List<String> apply(String token)  {
        return fst.apply(token);
    }

    public List<Map<String,String>> features(String analysis) {
        List<Map<String,String>> result = new ArrayList<Map<String,String>>();

        for (InflectionalGroup ig: splitAnalysis(analysis)) {
            result.add(addFeatures(ig.root, ig.pos, ig.inflections));
        }

        return result;
    }

    public static List<InflectionalGroup> splitAnalysis(String a) {
        List<InflectionalGroup> igs = new ArrayList<InflectionalGroup>();
        Matcher m = re.matcher(a);
        while (m.find()) {
            InflectionalGroup ig = new InflectionalGroup();
            ig.root = m.group(1);
            ig.pos = m.group(2);
            ig.inflections = m.group(3);
            igs.add(ig);
            String rest = m.group(4);
            m = re.matcher(rest);
        }

        return igs;
    }

    private Map<String,String> addFeatures(String root,
                                           String posTag,
                                           String infl) {
        String inflections[] = (infl == null || infl.length() == 0) ?
                null : infl.substring(1,infl.length()-1).split("><");
        Map<String,String> features = new HashMap<String,String>();

        String pos = posTag.replaceAll("^<", "").replaceAll("[:>].*", "");

        //defaults
        if (pos.equals("N") || pos.equals("Prn")) {
            features.put("Case", "Nom");
            features.put("Number", "Sing");
        } else if (pos.equals("Num")) {
            features.put("NumType", "Card");
        } else if (pos.equals("V")) {
            features.put("Number", "Sing");
            features.put("Negative", "Pos");
        }

        // TODO: the following conversion needs more work.
        for (int i = 0; inflections != null && i < inflections.length; i++) {
            if (inflections[i].equals("pl")) {
                features.put("Number", "Plur");
            } else if (inflections[i].equals("p1s")) {
                features.put("PersonPoss", "1");
                features.put("NumberPoss", "Sing");
            } else if (inflections[i].equals("p2s")) {
                features.put("PersonPoss", "2");
                features.put("NumberPoss", "Sing");
            } else if (inflections[i].equals("p3s")) {
                features.put("PersonPoss", "3");
                features.put("NumberPoss", "Sing");
            } else if (inflections[i].equals("p1p")) {
                features.put("PersonPoss", "1");
                features.put("NumberPoss", "Plur");
            } else if (inflections[i].equals("p2p")) {
                features.put("PersonPoss", "2");
                features.put("NumberPoss", "Plur");
            } else if (inflections[i].equals("p3p")) {
                features.put("PersonPoss", "3");
                features.put("NumberPoss", "Plur");
            } else if (inflections[i].equals("acc")) {
                features.put("Case", "Acc");
            } else if (inflections[i].equals("dat")) {
                features.put("Case", "Dat");
            } else if (inflections[i].equals("loc")) {
                features.put("Case", "Loc");
            } else if (inflections[i].equals("abl")) {
                features.put("Case", "Abl");
            } else if (inflections[i].equals("gen")) {
                features.put("Case", "Gen");
            } else if (inflections[i].equals("ins")) {
                features.put("Case", "Ins");
            } else if (inflections[i].equals("1s")) {
                features.put("Person", "1");
                features.put("Number", "Sing");
            } else if (inflections[i].equals("2s")) {
                features.put("Person", "2");
                features.put("Number", "Sing");
            } else if (inflections[i].equals("3s")) {
                features.put("Person", "3");
                features.put("Number", "Sing");
            } else if (inflections[i].equals("1p")) {
                features.put("Person", "1");
                features.put("Number", "Plur");
            } else if (inflections[i].equals("2p")) {
                features.put("Person", "2");
                features.put("Number", "Plur");
            } else if (inflections[i].equals("3p")) {
                features.put("Person", "3");
                features.put("Number", "Plur");
            } else if (inflections[i].equals("pers")) {
                features.put("PronType", "Pers");
                features.put("Pos", "PersP");
            } else if (inflections[i].equals("loc")) {
                features.put("PronType", "Loc");
//                features.put("Pos", "LocP");
            } else if (inflections[i].equals("dem")) {
                features.put("PronType", "Dem");
                features.put("Pos", "DemonsP");
            } else if (inflections[i].equals("qst")) {
                features.put("PronType", "Int");
                features.put("Pos", "QuesP");
            } else if (inflections[i].equals("refl")) {
                features.put("Reflex", "Yes");
                features.put("Pos", "ReflexP");
            } else if (inflections[i].equals("ord")) {
                features.put("NumType", "Ord");
                features.put("Pos", "Card");
            } else if (inflections[i].equals("dist")) {
                features.put("NumType", "Dist");
                features.put("Pos", "Ord");
// TODO: TAM mapping this is incomplete
            } else if (inflections[i].equals("past")) {
                features.put("Tense", "Past");
                features.put("Aspect", "Perf");
            } else if (inflections[i].equals("evid")) {
                features.put("Tense", "Past");
                features.put("Evidential", "Nfp");
                features.put("Aspect", "Perf");
            } else if (inflections[i].equals("fut")) {
                features.put("Tense", "Fut");
                features.put("Aspect", "Perf");
            } else if (inflections[i].equals("aor")) {
                features.put("Tense", "Pres");
                features.put("Aspect", "Habit");
                features.put("Mood", "Gen");
            } else if (inflections[i].equals("cont")) {
                features.put("Tense", "Pres");
                features.put("Aspect", "Prog");
            } else if (inflections[i].equals("impf")) {
                features.put("Tense", "Pres");
                features.put("Aspect", "Prog");
            } else if (inflections[i].equals("obl")) {
                features.put("Tense", "Pres");
                features.put("Mood", "Nec");
            } else if (inflections[i].equals("cond")) {
                features.put("Tense", "Pres");
                features.put("Mood", "Cnd");
            } else if (inflections[i].equals("imp")) {
                features.put("Tense", "Pres");
                features.put("Mood", "Imp");
            } else if (inflections[i].equals("opt")) {
                features.put("Tense", "Pres");
                features.put("Mood", "Opt");
            } else if (inflections[i].equals("cpl:pres")) {
                features.put("Tense", "Pres");
            } else if (inflections[i].equals("cpl:cond")) {
                features.put("Mood", "Cnd");
            } else if (inflections[i].equals("cpl:past")) {
                if(features.containsKey("Tense")) {
					if (features.get("Tense").equals("Past")) {
						features.put("Tense", "Pqp");
					} else if (features.get("Tense").equals("Fut")){
						features.put("Tense", "Pfut");
					} 
				}
				else {
                    features.put("Tense", "Past");
                }
            } else if (inflections[i].equals("cpl:evid")) {
                if (features.containsKey("Tense")) {
                    features.put("Evidential", "Nfp");
                } else {
                    features.put("Evidential", "Nfp");
                    features.put("Tense", "Past");
                }

            } else if (inflections[i].equals("dir")) {
                features.put("Mood", "Gen");
            } else if (inflections[i].equals("neg")) {
                features.put("Negative", "Neg");
            } else if (inflections[i].equals("prop")) {
                features.put("Pos", "Prop");
            } else if (inflections[i].equals("def")) {
                features.put("Definite", "Def");
            } else if (inflections[i].equals("indef")) {
                features.put("Definite", "Ind");
            } else if (inflections[i].equals("qst")) {
                features.put("Pos", "QuesP");
            } else {
                Logger.getLogger(TrMorph.class.getName())
                    .log(Level.WARNING, "Ignoring unhandled inflection `" + inflections[i] + "'.");
            }
        }
        
        // TODO: map non-root lammas to more intelligible labels
        features.put("Lemma", root);
        if (!features.containsKey("Pos")) {
            features.put("Pos", posMapMS.get(pos));
        }
        return features;
    }

}
