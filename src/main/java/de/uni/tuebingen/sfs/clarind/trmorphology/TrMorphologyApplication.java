package de.uni.tuebingen.sfs.clarind.trmorphology;

import de.uni.tuebingen.sfs.clarind.trmorphology.resources.IndexResource;
import de.uni.tuebingen.sfs.clarind.trmorphology.resources.TrMorphologyResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Turkish morphology web service application.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */
public class TrMorphologyApplication extends Application<TrMorphologyConfiguration> {
    public static void main(String[] args) throws Exception{
        new TrMorphologyApplication().run(args);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void initialize(Bootstrap<TrMorphologyConfiguration> bootstrap) {
    }

    @Override
    public void run(TrMorphologyConfiguration configuration, Environment environment) throws Exception {
        TrMorphologyResource trMorphologyResource = new TrMorphologyResource();
        IndexResource indexResource = new IndexResource();
        environment.jersey().register(trMorphologyResource);
        environment.jersey().register(indexResource);
    }
}
