package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;

/**
 * Inflectional groups are sub-word syntactic units used in Turkish
 * NLP.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */

class InflectionalGroup
{
    public String root;
    public String pos;
    public String inflections;
}
