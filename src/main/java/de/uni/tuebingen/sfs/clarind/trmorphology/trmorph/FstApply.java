package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

/**
 * Example program for using Fst class.
 *
 */
public class FstApply
{
    public static void main( String[] args ) throws IOException
    {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(System.in));
        Fst trmorph = new Fst(FstApply.class
                .getResourceAsStream("/trmorph.att"));

        String line = null;
        while ((line = in.readLine()) != null) {
            List<String> analyses = trmorph.apply(line);
            if (analyses != null) {
                for (String a: analyses) {
                    System.out.println(line + "\t" + a);
                }
            } else {
                    System.out.println(line + "\t+?");
            }
            System.out.println();
        }
    }
}
