service-trmorphology 

# What is it?

This service combines Turkish morphological analysis and part of
speech tagging. 

## Input

Input is a TCF file with text layer. Not that the service requires
tokens and sentences layers _not_ to be present. 

## Output layers

- tokens
- sentences
- pos
- morphology

## How to test

 You can test this webservice using either "wget" or "curl" along with the input 
 provided as part of this project at src/main/webapp/input.xml

 Run wget:
 wget --post-file=input.xml --header='Content-Type: text/tcf+xml' http://localhost:8080/service-nentities/annotate/stream

 Or run curl:
 curl -H 'content-type: text/tcf+xml' -d @input.xml -X POST http://localhost:8080/service-nentities/annotate/stream
 
